package main

import (
	"fmt"
	"bufio"
	"os"
	"strconv"
	"strings"
)

func main() {

	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter value 1: ")
	val1, _ := reader.ReadString('\n')
	aFloat, _ := strconv.ParseFloat(strings.TrimSpace(val1), 64)

	fmt.Print("Enter value 2: ")
	val2, _ := reader.ReadString('\n')
	aFloat2, _ := strconv.ParseFloat(strings.TrimSpace(val2), 64)

	fmt.Println("The sum is: ", aFloat + aFloat2)

}

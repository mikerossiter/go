package main

import (
	"fmt"
	"time"
)

//Collatz conjecture function starts here
func collatz(val1 int) {
	for val1 >= 1 {
		if val1%2 == 0 {
			val1 = val1 / 2
			fmt.Printf("%d\n", val1)
			// Sleep used as I like to see the calculations!
			time.Sleep(100 * time.Millisecond)

		} else {
			val1 = val1*3 + 1
			fmt.Printf("%d\n", val1)
			time.Sleep(100 * time.Millisecond)

		}

		if val1 == 1 {
			fmt.Println("Done!")
			break
		}

	}
}

//Input for accepting the integer
func main() {

	fmt.Print("Enter an integer: ")
	var val1 int
	fmt.Scanln(&val1)

	collatz(val1)
}

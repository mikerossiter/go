package main

import (
	"fmt"
)

func main() {
	fmt.Print("Enter a measurement in feet: ")
	var input float64
	fmt.Scanf("%f", &input)

	//Takes input and converts to meters
	fmt.Printf("%v feet is %.2fm\n", input, input*0.3048)
}

package main

import (
	"fmt"
)

func main() {
	fmt.Print("Enter a temperature in Fahrenheit to convert into Celsius: ")
	var input float64
	fmt.Scanf("%f", &input)

	//output := (input - 32) * 5 / 9
	fmt.Printf("%v degrees Fahrenheit is %.2f degrees Celsius\n", input, (input-32)*5/9)
}

package main

import (
	"fmt"
	"time"
)

func main() {
	i := 1
	for i <= 10 {
		fmt.Println(i)
		time.Sleep(1 * time.Second)
		i += 1
	}
	fmt.Println("END!")
}

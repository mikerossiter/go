package main

import (
	"fmt"
)

//Euclidean function starts here to find the GCD of both numbers
func euclidean(val1, val2 int) int {
	for val2 != 0 {
		x := val2
		val2 = val1 % val2
		val1 = x
	}
	return val1
}

//Input for accepting the integers
func main() {

	fmt.Print("Enter first integer: ")
	var val1 int
	fmt.Scanln(&val1)

	fmt.Print("Enter second integer to find the greatest common divisor: ")
	var val2 int
	fmt.Scanln(&val2)
	fmt.Println(euclidean(val1, val2))
}
